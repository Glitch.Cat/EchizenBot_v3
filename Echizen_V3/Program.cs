﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;

namespace Echizen_V3
{
    class Program
    {
        public static DiscordClient discord;
        static CommandsNextModule commands;

        static void Main(string[] args)
        {
            MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        //Framework of program
        static async Task MainAsync(string[] args)
        {
            discord = new DiscordClient(new DiscordConfig
            {
                Token = Key.tokenKey,
                TokenType = TokenType.Bot,

                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug
            });

            //Message Responding
            discord.MessageCreated += async e =>
            {
                //Prevents Bot from responding to Bots (or itself, in which case, infinite loop)
                if (!e.Message.Author.IsBot)
                {
                    if (e.Message.Content.ToLower().StartsWith("/o/"))
                        await e.Message.RespondAsync("\\o\\");

                    if (e.Message.Content.ToLower().StartsWith("\\o\\"))
                        await e.Message.RespondAsync("/o/");
                }

            };

            //Command Prefix
            commands = discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefix = "."
            });

            //Connect 'CommandList' class with main class
            commands.RegisterCommands<CommandList>();

            //Ready Event
            discord.DebugLogger.LogMessage(LogLevel.Info, "Echizen", "Initializing Ready", DateTime.Now);

            discord.Ready += async e =>
            {
                await Task.Yield(); //screw green squiggly lines :niko:
                discord.DebugLogger.LogMessage(LogLevel.Info, "Echizen", "Ready! Setting status message..", DateTime.Now);
                //playing status
                await discord.UpdateStatusAsync(new Game("with Ari"));
            };

            discord.DebugLogger.LogMessage(LogLevel.Info, "Echizen", "Connecting..", DateTime.Now);
            await discord.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}

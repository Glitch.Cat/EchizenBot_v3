﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace Echizen_V3
{
    public class CommandList
    {
        //Greets user who issued command
        [Command("greet")]
        [Description("Greets the user who issues this command.")]
        public async Task Greet(CommandContext ctx)
        {
            //lets user know bot is responding via typing trigger
            await ctx.TriggerTypingAsync();

            await ctx.RespondAsync("o/ " + ctx.User.Mention + ".");
        }

        //Picks random number between 2 integers that user inputs
        [Command("random")]
        [Description("Picks random number between 2 integers that the user inputs.\n i.e. `.random 1 6`")]
        public async Task Random(CommandContext ctx, int min, int max)
        {
            //typing trigger
            await ctx.TriggerTypingAsync();

            var rnd = new Random();
            await ctx.RespondAsync("Your random number is: " + rnd.Next(min,max));
        }

        //Purge command, only available to admin :original>>: Command(purge), Hidden, RequiredPermissions()
        [Command("purge"), RequirePermissions(Permissions.Administrator)]
        [Description("*Deletes a set amount of messages that user inputs.\n i.e. `.purge 1` \n *Note: only available to Admin.")]
        //testing for feedback on pre-req not being met
        
        //end
        public async Task Purge(CommandContext ctx, int quantity)
        {
            //boolean to provide feedback when command is used by user
            //without required permissions
            try
            {
                IReadOnlyCollection<DiscordMessage> messages = await ctx.Channel.GetMessagesAsync(quantity);
                var messagelist = messages;
                await ctx.Channel.DeleteMessagesAsync(messagelist);

                await ctx.RespondAsync("Messages Deleted!");
            }

            catch(Exception)
            {
                await ctx.RespondAsync("no permission");
            }



        }

        //logging commands executed (test)
        
    }
}
